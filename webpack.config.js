/**
 * webpack config
 */
var path = require('path');
var webpack = require('webpack');
var node_modules = __dirname + '/node_modules';
var bower_components = __dirname + '/bower_components';
var reactPath = node_modules + '/react/react.js';
var reactDomPath = node_modules + '/react-dom/dist/react-dom.js';
//*** Note - unable to resolve react-dom to skip building at this time ***


module.exports = {
	entry: {
		app: ['./src/main.js'],
		vendors: ['react']
	},
	resolve: {
		alias: {},
		root: [bower_components]
	},
	output: {
		path: __dirname + '/public/js',
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				exclude: /(node_modules|bower_components)/,
				query: {
					presets: ['es2015', 'react']
				}
			},
			{
				test: /\.scss$/,
				loader: 'style-loader!css-loader!sass-loader'
			},
			{
				test: /\.(png|jpg|jpeg)$/,
				loader: 'url?limit=25000'
			}
		],
		noParse: []
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
    	jQuery: "jquery",
    	"window.jQuery": "jquery"
		}),
		new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
	]	
};