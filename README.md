## React + Webpack Starter

### Purpose
The purpose of this project is to provide a starting point for creating React apps. 
This project follows the popular Node setup with React bundled with Webpack with es6 enabled via babel.

### Sources
This project setup is a compilation of several resources.  The following were the most up-to-date and correct:
- [Webpack Docs](http://webpack.github.io/docs/tutorials/getting-started/#config-file)
- [Webpack-dev-server docs](https://webpack.github.io/docs/webpack-dev-server.html)
- [React Webpack cookbook](https://christianalfoni.github.io/react-webpack-cookbook/index.html)
- [Twilio blog post](https://www.twilio.com/blog/2015/08/setting-up-react-for-es6-with-webpack-and-babel-2.html)
- [Thoughtbot article](https://robots.thoughtbot.com/setting-up-webpack-for-react-and-hot-module-replacement)

### Known Issues

There are a few issues to be aware of with this setup. The current version of react is 0.14.2 and the new react-dom npm package can cause conflicts in the webpack configuration.

The following are some issues:

* Unable to prevent building of both react and react-dom packages during the use of webpack; must currently build all resources

* Unable to get sass/css hot loading to work with current setup; must rebuild css with webpack

* Unable to get Bower components to be included in vendors bundle