import React from 'react';
import ReactDOM from 'react-dom';

class Body extends React.Component {
	render() {
		return (
      <div id="content">
        <div className="row">
          <div className="cell auto-size">
            <div className="panel">
                <div className="heading">
                    <span className="title">Panel Title</span>
                </div>
                <div className="content">
                    ... panel content ...
                </div>
            </div>
          </div>
        </div>
      </div>
		);
	}
}

ReactDOM.render(<Body/>, document.getElementById('layout-body'));