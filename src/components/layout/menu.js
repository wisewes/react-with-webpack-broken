import React from 'react';
import ReactDOM from 'react-dom';

class Menu extends React.Component {
	render() {
		return (
			<div className="app-bar">
				<a className="app-bar-element" href="...">
					<span className="mif-home"></span> React + Webpack</a>
				<span className="app-bar-divider"></span>
			</div>
		);
	}
}

ReactDOM.render(<Menu/>, document.getElementById('layout-menu'));