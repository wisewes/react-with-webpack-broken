import React from 'react';
import ReactDOM from 'react-dom';

class Heading extends React.Component {
	render() {
		return (
			<div className="row">
				<div className="cell">
					<h1>React + Webpack<small>with MetroUI</small></h1>
				</div>
			</div>
		);
	}
}

ReactDOM.render(<Heading/>, document.getElementById('heading-comp'));