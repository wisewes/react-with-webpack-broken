import React from 'react';
import ReactDOM from 'react-dom';

class Footer extends React.Component {
	render() {
		return (
			<footer className="row ribbed-dark">
				<p>This is the footer text</p>
			</footer>
		);
	}
}

ReactDOM.render(<Footer/>, document.getElementById('layout-footer'));