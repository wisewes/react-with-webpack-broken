/**
 * main.js
 * This is the entry file to webpack bundle
 * Refer to webpack.config.js for setup
 */

/* libs */
import _ from 'underscore';

/* React Components */
import Menu from './components/layout/menu.js';
import HeadingComp from './components/layout/heading.js';
import Body from './components/layout/body.js';
import Footer from './components/layout/footer.js';

/* styles */
import './styles/main.scss';